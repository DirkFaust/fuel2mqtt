package main

import (
	"os"
	"time"

	"github.com/joho/godotenv"
	log "github.com/sirupsen/logrus"
	"gitlab.com/DirkFaust/faustility/pkg/conversion"
	"gitlab.com/DirkFaust/faustility/pkg/env"
	"gitlab.com/DirkFaust/faustility/pkg/mqtt"
	"gitlab.com/DirkFaust/fuel2mqtt/pkg/tankerkoenig"
)

const (
	envAPIKey   = "TANKERKOENIG_API_KEY"
	envLat      = "POSITION_LAT"
	envLon      = "POSITION_LON"
	envRadius   = "SEARCH_RADIUS"
	envPeriod   = "PERIOD"
	envMqttHost = "MQTT_HOST"
	envMqttPort = "MQTT_PORT"
)

func main() {
	log.SetFormatter(&log.TextFormatter{FullTimestamp: true})
	log.SetOutput(os.Stdout)
	log.SetLevel(log.DebugLevel)

	_ = godotenv.Load()
	apiKey := env.GetOrDie(envAPIKey)

	log.Info("Starting")

	var lat, lon, radius float64
	var mqttPort uint16

	conversion.FromStringOrDie(env.GetOrDie(envLat), &lat)
	conversion.FromStringOrDie(env.GetOrDie(envLon), &lon)
	conversion.FromStringOrDie(env.GetOrDie(envRadius), &radius)
	conversion.FromStringOrDie(env.GetOrDie(envMqttPort), &mqttPort)

	mqttHost := env.GetOrDie(envMqttHost)
	requestPeriod, err := time.ParseDuration(env.GetOrDie(envPeriod))
	if err != nil {
		panic("Unable to decode the give PERIOD ENV")
	}
	quotes := make(chan tankerkoenig.FuelQuote)
	kill := make(chan bool)

	forwarder, err := mqtt.NewForwarderBuilder[tankerkoenig.FuelQuote]().
		Channels().Data(quotes).KillWitch(kill).
		Topics().Input("fuel2mqtt/command").Output("fuel2mqtt/quote").
		Server().Host(mqttHost).Port(uint16(mqttPort)).
		Build()

	if err != nil {
		log.Errorf("Unable to build MQTT forwarder: %v", err)
		os.Exit(1)
	}

	setup, err := tankerkoenig.NewSetupBuilder(apiKey).WithCoordinates(lat, lon).WithRadius(radius).WithPeriod(requestPeriod).Build()
	if err != nil {
		panic(err)
	}

	go tankerkoenig.Task(quotes, kill, *setup)

	forwarder.Run()

}
