package tankerkoenig

import (
	"fmt"
	"time"
)

type Setup struct {
	apiKey string
	lat    *float64
	lon    *float64
	radius *float64
	period time.Duration
}

type SetupBuilder struct {
	setup Setup
}

func NewSetupBuilder(apiKey string) *SetupBuilder {
	return &SetupBuilder{setup: Setup{
		apiKey: apiKey,
		period: 5 * time.Minute,
	}}
}

func (b *SetupBuilder) WithCoordinates(lat, lon float64) *SetupBuilder {
	b.setup.lat = &lat
	b.setup.lon = &lon
	return b
}

func (b *SetupBuilder) WithRadius(kilometers float64) *SetupBuilder {
	b.setup.radius = &kilometers
	return b
}

func (b *SetupBuilder) WithPeriod(period time.Duration) *SetupBuilder {
	b.setup.period = period
	return b
}

func (b *SetupBuilder) Build() (*Setup, error) {
	if b.setup.lat == nil || b.setup.lon == nil {
		return nil, fmt.Errorf("need at least coordinates")
	}

	return &b.setup, nil
}
