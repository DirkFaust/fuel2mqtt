package tankerkoenig

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	resty "github.com/go-resty/resty/v2"
	log "github.com/sirupsen/logrus"
)

type FuelQuote struct {
	Name   string             `json:"name"`   //< Name of the fuel station
	Lat    float64            `json:"lat"`    //< Latitude of the fuel station
	Lon    float64            `json:"lon"`    //< Longitude of the fuel station
	Prices map[string]float64 `json:"prices"` //< Prices of the fuel station, indexed by fuel type
}

func (quote FuelQuote) Serialize() (*[]byte, error) {
	result, err := json.Marshal(quote)
	if err != nil {
		return nil, err
	}

	return &result, nil
}

// Sleeps the given `duration` and signals the completion via the sleepDone channel
func sleeper(ctx context.Context, duration time.Duration, sleepDone chan bool) {
	timer := time.NewTimer(duration)
	select {
	case <-ctx.Done():
		log.Debug("Timer done")
		if !timer.Stop() {
			log.Info("Timer could not be stopped. Draining.")
			<-timer.C
		}
	case <-timer.C:
	}
	log.Debug("Timer signalling finish")
	sleepDone <- true
}

type station struct {
	Brand  string  `json:"brand"` //< station brand/name
	Place  string  `json:"place"` //< city name
	Diesel float64 `json:"diesel"`
	E5     float64 `json:"e5"`
	E10    float64 `json:"e10"`
	IsOpen bool    `json:"isOpen"`
}

type response struct {
	Status   string `json:"status"`
	Stations []station
}

func RequestPrices(setup *Setup) (*response, error) {
	// https://creativecommons.tankerkoenig.de/json/list.php?lat=52.521&lng=13.438&rad=1.5&sort=dist&type=all&apikey=00000000-0000-0000-0000-000000000002

	client := resty.New()
	resp, err := client.R().
		SetQueryParams(map[string]string{
			"lat":    fmt.Sprintf("%.4f", *setup.lat),
			"lng":    fmt.Sprintf("%.4f", *setup.lon),
			"rad":    fmt.Sprintf("%.4f", *setup.radius),
			"sort":   "dist",
			"type":   "all",
			"apikey": setup.apiKey,
		}).
		SetHeader("Accept", "application/json").
		Get("https://creativecommons.tankerkoenig.de/json/list.php")

	if err != nil {
		return nil, fmt.Errorf("unable to request prices: %v", err)
	}

	var result response
	uerr := json.Unmarshal(resp.Body(), &result)
	if uerr != nil {
		return nil, fmt.Errorf("unable to unmarshal quotes response: %v", uerr)
	}

	return &result, nil
}

func Task(quotes chan FuelQuote, kill chan bool, setup Setup) {
	sleepDone := make(chan bool)
	for {
		prices, err := RequestPrices(&setup)
		if err != nil {
			log.Fatalf("unable to request prices: %v", err)
		} else {
			log.Debugf("Got prices: %v", prices)
			for _, quote := range prices.Stations {
				if !quote.IsOpen {
					continue
				}
				fuelQuote := FuelQuote{
					Name:   quote.Brand + " " + quote.Place,
					Prices: make(map[string]float64),
				}
				if quote.Diesel > 0 {
					fuelQuote.Prices["diesel"] = quote.Diesel
				}
				if quote.E5 > 0 {
					fuelQuote.Prices["e5"] = quote.E5
				}
				if quote.E10 > 0 {
					fuelQuote.Prices["e10"] = quote.E10
				}

				quotes <- fuelQuote
			}
		}

		ctx, cancel := context.WithCancel(context.Background())
		go sleeper(ctx, setup.period, sleepDone)
		select {
		case <-sleepDone:
			continue
		case <-kill:
			log.Info("Exiting requestLoop for killswitch, canceling the sleep for next request")
			cancel()
			return
		}
	}
}
