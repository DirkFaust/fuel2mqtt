# fuel2mqtt

A simple wrapper around the tankerkoenig.de API to periodically get fuel prices and publish them via MQTT

Quotes for fuel prices will be published towards the topic `fuel2mqtt/quotes` in the following JSON format:

```
{
    "name": "Tankstelle Foo",   //< name of the gas station with place
    "lat": 1.234,               //< latitude of the gas station
    "lon": 2.345,               //< longitude of the gas station>
    "prices": {                 //< map of prices
        "diesel": 2.99,
        "e5": 2.99,
        "e10": 2.98
    }
}
```

Price quotes with a value of zero (0) are not published, as well as no publishes will be made for currently closed gas stations.
